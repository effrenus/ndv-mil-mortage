import gulp from 'gulp';
import svgstore from 'gulp-svgstore';
import svgmin from 'gulp-svgmin';

gulp.task('svg', function () {
    return gulp
        .src('app/images/svg-sprites/*.svg')
        // .pipe(svgmin())
        .pipe(svgstore({ inlineSvg: true }))
        .pipe(gulp.dest('app/images'));
});