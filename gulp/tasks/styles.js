import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import csscomb from 'gulp-csscomb';
import browserSync from 'browser-sync';
import plumber from 'gulp-plumber';
import errorHandler from '../errorHandler';

gulp.task('styles', () => {
	return gulp
		.src('app/styles/default.sass')
		.pipe(plumber({errorHandler}))
		.pipe(sass())
		.pipe(csscomb())
		.pipe(autoprefixer({
			browsers: ['last 3 versions', 'iOS >= 7'],
			cascade: false,
		}))
		.pipe(gulp.dest('dist/css'))
		.pipe(browserSync.stream());
});
