import loadJS from './utils/loadJS';
import map from './map';
import Dropdown from './MapDropdown';

const section = {
	initialize: function initialize() {
		this.container = document.querySelector('.map');
		this.isOpen = false;

		$('#page-mode').on('mouseover click', '.slide-delim__label', function pageDelimOver() {
			(section.isOpen ? section.close() : section.open());
		});

		// Подгружаем карту с задержкой, чтобы не не было подтормаживания
		loadJS('https://api-maps.yandex.ru/2.1/?lang=ru_RU', () => {
			ymaps.ready(() => this.onLoadMap());
		});
	},

	onLoadMap: function onLoadMap() {
		this.modeItems = Array.prototype.slice.call(this.container.querySelectorAll('.map-mode__item'));
		map.initialize();

		const unitDropdown = new Dropdown(
			points.objects.map((item, i) => { return {text: item.properties.title, value: i}; }),
			{container: '.select-units', defaultText: 'Выберите объект'}
		);
		unitDropdown.on('change', map.openBalloon);

		const officeDropdown = new Dropdown(
			points.offices.map((item, i) => { return {text: item.properties.metro, value: i}; }),
			{container: '.select-offices', defaultText: 'Выберите станцию метро'}
		);
		officeDropdown.on('change', map.openBalloon);

		this.bindHandlers();
		this.setMode('unit');
	},

	bindHandlers: function bindHandlers() {
		this.container.querySelector('.map-mode').addEventListener('click', this.onTabClick.bind(this));
	},

	open: function open() {
		this.isOpen = true;
		// utils.transitionToClass(this.container, 'is-open', 'is-animate').then(() => { this.isOpen = true; });
		$(this.container).addClass('is-open');
		setTimeout(() => {
			$(this.container).addClass('is-animate');
		}, 10);
	},

	close: function close() {
		this.isOpen = false;
		$(this.container).one('transitionend', () => {
			$(this.container).removeClass('is-open');
		});
		$(this.container).removeClass('is-animate');
	},

	setMode: function setMode(type) {
		const cls = this.container.className;
		this.container.className = cls.replace(/\s+(mode-unit|mode-office)/ig, '') + ` mode-${type}`;
		map.setMode(type);
		map.renderPoints();
	},

	onTabClick: function onTabClick(event) {
		const type = event.target.getAttribute('data-type');
		this.setMode(type);
	},
};

export default section;
