import assign from 'object-assign';
import EventEmitter from 'events';

const template = `
<div class="modal-window">
	<div class="modal-window__container">
		<div class="modal-window__content"></div>
		<span class="modal-window__close">&times;</span>
	</div>
</div>
`;

class Modal extends EventEmitter {
	constructor() {
		super();
		this.isOpen = false;
		this.createDom();
		this.bindHandlers();
	}

	createDom() {
		this.$dom = $(template);
		$(document.body).append(this.$dom);
	}

	append(element) {
		let $element = $(element);
		this.$dom.find('.modal-window__content').html($element);
	}

	open() {
		this.isOpen = true;
		this.$dom.addClass('is-open');
	}

	close() {
		this.isOpen = false;
		this.$dom.removeClass('is-open');
	}

	bindHandlers() {
		$(window).keyup((event) => {
			if (event.keyCode === 27 && this.isOpen) {
				this.close();
			}
		});
		this.$dom
				.on('click', '.modal-window__close', this.close.bind(this));
	}
}

let modal = new Modal();

export default modal;