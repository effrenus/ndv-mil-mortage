import Actions from './Actions';

export default {
	initialize: function initialize() {
		const slideActions1 = new Actions('.slide:eq(0) .card');
		const slideActions2 = new Actions('.slide:eq(1) .card');
		const slideActions3 = new Actions('.slide:eq(2) .card');

		$('.cam-popup').on('click', '.cam-popup__type span', function camTabClick() {
			const $elm = $(this);
			const $parent = $elm.parents('.cam-popup');
			$parent.find('.cam-popup__type span').removeClass('is-active');

			$elm.addClass('is-active');
			$parent
					.find('.cam-popup__list').removeClass('is-active')
					.eq(parseInt($elm.attr('data-num'), 10)).addClass('is-active');
		});

		$('.cam-popup').on('click', '.cam-popup__label', function camLabelClick() {
			const $parent = $(this).parents('.cam-popup');
			$parent.find('.cam-popup__label').removeClass('is-active');
			$(this).addClass('is-active');

			const url = $(this).attr('data-src') || 'about:blank';
			$parent.find('iframe').attr('src', url);
		});
	},
};
