import assign from 'object-assign';

require('../plugins/fancybox.js');

const defaultOptions = {
	width: '665px',
	height: '490px',
	type: 'iframe',
	openEffect: 'fade',
	closeEffect: 'fade',
	padding: 0,
	helpers: {
		buttons: {},
	},
};

class Actions {
	constructor(container) {
		this.container = $(container);
		this.bindHandlers();
	}

	bindHandlers() {
		const $video = $('.action-video', this.container);
		$video.fancybox(assign({}, defaultOptions, { href: $video.attr('href') }));

		const $perspective = $('.action-perspective', this.container);
		$perspective.fancybox(assign({}, defaultOptions, { href: $perspective.attr('href') }));

		$('.action-camera', this.container).fancybox(
			{
				scrolling: 'visible',
				padding: 0,
				afterShow: function afterShow() {
					const url = this.content.find('.cam-popup__label.is-active').attr('data-src');
					this.content.find('iframe').attr('src', url);
				},
				afterClose: function afterClose() {
					this.content.find('iframe').attr('src', 'about:blank');
				},
			});
	}
}

export default Actions;
