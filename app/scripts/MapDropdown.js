import assign from 'object-assign';
import EventEmitter from 'events';

const defaultOptions = {
	'classPrefix': 'select',
};

class Dropdown extends EventEmitter {
	constructor(items = [], options = {}) {
		super();
		this.options = assign({}, options, defaultOptions);
		this.$container = $(this.options.container);
		this.items = items;
		this.isOpen = false;
		this.constuctDom();
		this.bindHandlers();
	}

	constuctDom() {
		const $select = this.$select = $('<div class="select"/>');
		$select.append($('<div class="select__button"/>'));
		$select.append($('<div class="select__text"/>').text(this.options.defaultText));
		const $dropdown = $('<div class="select__dropdown"/>');
		this.items.forEach(item => {
			const $item = $('<div class="select__item"/>');
			$item.text(item.text).attr('data-value', item.value);
			$dropdown.append($item);
		});
		$select.append($dropdown);
		this.$container.append($select);
	}

	bindHandlers() {
		this.$select.find('.select__button').click(this.toggleState.bind(this));
		this.$select.on('click', '.select__item', this.onItemClick.bind(this));
	}

	toggleState() {
		this.isOpen = !this.isOpen;
		this.$select.toggleClass('is-open');
	}

	onItemClick(event) {
		const $item = $(event.target);
		this.$select.find('.select__text').text($item.text());
		this.emit('change', $item.attr('data-value'));
		this.toggleState();
	}
}

export default Dropdown;
