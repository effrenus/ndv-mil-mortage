require('./plugins/jquery.maskedinput.js');

const application = {
	initialize: function initialize() {
		this.isOpen = false;
		this.container = document.querySelector('.application');
		this.form = this.container.querySelector('form');
		this.url = this.form.getAttribute('action');
		this.fields = Array.prototype.slice.call(this.form.querySelectorAll('input, textarea'));
		this.bindHandlers();
	},

	bindHandlers: function bindHandlers() {
		this.onSubmit = this.onSubmit.bind(this);
		this.toggleState = this.toggleState.bind(this);

		this.form.addEventListener('submit', this.onSubmit, false);
		this.fields.forEach(field => {
			if (field.type === 'tel') {
				$(field).mask('+7 (999) 999-99-99');
			}

			if (field.value === '') {
				field.className += ' is-empty';
			}

			field.addEventListener('blur', event => {
				const elm = event.target;
				let cls = elm.className.replace(/\s+is-empty/ig, '');
				if (elm.value === '') {
					cls += ' is-empty';
				}
				elm.className = cls;
			});
		});
		this.container.querySelector('.close').addEventListener('click', this.toggleState, false);
		document.addEventListener('click', event => {
			if (event.target.className.indexOf('request-button') === -1) {
				return true;
			}
			this.toggleState(event);
		}, false);
	},

	toggleState: function toggleState(event) {
		event.stopPropagation();
		event.preventDefault();

		(application.isOpen ? application.close() : application.open());
	},

	open: function open() {
		this.isOpen = true;
		$(this.container).addClass('is-open');
		setTimeout(() => {
			$(this.container).addClass('is-animate');
		}, 10);
	},

	close: function close() {
		this.isOpen = false;
		$(this.container).one('transitionend', () => {
			$(this.container).removeClass('is-open');
		});
		$(this.container).removeClass('is-animate');
	},

	onSubmit: function onSubmit(event) {
		event.stopPropagation();
		event.preventDefault();

		if (this.valid()) {
			this.sendData();
		}
	},

	sendData: function sendData() {
		const xhr = new XMLHttpRequest();
		xhr.open('POST', this.url, true);
		xhr.onreadystatechange = () => {
			if (xhr.readyState !== 4) {
				return;
			}
			if (xhr.status === 200) {
				this.onSuccess();
			} else {
				this.onError();
			}
		};
		xhr.send(new FormData(this.form));
	},

	onSuccess: function onSuccess() {
		const $message = $('<div class="message success">Ваша заявка принята</div>');
		$(this.form).find('h2').after($message);

		setTimeout(() => {
			this.close();
			$(this.form).find('.message').remove();
		}, 3000);

		this.clearForm();
	},

	onError: function onError() {
		const $message = $('<div class="message error">Извините, произошла ошибка. Попробуйте еще раз.</div>');
		$(this.form).find('h2').after($message);

		setTimeout(() => {
			$(this.form).find('.message').remove();
		}, 6000);
	},

	clearForm: function clearForm() {
		this.form.reset();
		$(this.fields).addClass('is-empty');
	},

	valid: function valid() {
		const errors = [];
		this.fields.forEach(field => {
			if (field.required && field.value === '') {
				field.parentNode.className += ' is-invalid';
				errors.push('Не заполнено поле');
			}
		});

		return errors.length ? false : true;
	},
};

export default application;
