import layouts from './layouts';
import assign from 'object-assign';


const map = {
	initialize: function initialize() {
		this.container = document.getElementById('map-viewport');
		this.geoObjects = {objects: [], offices: []};
		this.renderMap();
		layouts.create();

		this.createMapPoints(points);
	},

	openBalloon: function openBalloon(id) {
		const collection = map.type === 'unit' ? map.geoObjects.objects : map.geoObjects.offices;
		const num = parseInt(id, 10);

		collection.some((point, i) => {
			if (i === num) {
				point.balloon.open();
				return true;
			}
		});
	},

	setMode: function setMode(type) {
		this.type = type;
	},

	renderMap: function renderMap() {
		this.map = new ymaps.Map(this.container, {
			center: [55.76, 37.64],
			zoom: 10,
			controls: [],
		});
		this.map.setType('yandex#hybrid');
	},

	createMapPoints: function createMapPoints(points) {
		points.objects.forEach(point => {
			const placemark = new ymaps.Placemark(point.coords, point.properties || {}, assign({}, layouts.balloonOptions, point.icon || {}));
			this.geoObjects.objects.push(placemark);
		});

		points.offices.forEach(point => {
			const placemark = new ymaps.Placemark(point.coords, point.properties || {}, assign({}, layouts.balloonOptions, layouts.defaultIcon));
			this.geoObjects.offices.push(placemark);
		});
	},

	renderPoints: function renderPoints() {
		const collection = this.type === 'unit' ? this.geoObjects.objects : this.geoObjects.offices;
		this.map.geoObjects.removeAll();
		collection.forEach(point => {
			this.map.geoObjects.add(point);
		});
	},
};

export default map;
